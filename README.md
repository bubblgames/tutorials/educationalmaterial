# Game Dev Learning Material

## Online Reading
- [Game Programming Patterns](https://gameprogrammingpatterns.com/)
- [3D Math for Games](https://gamemath.com/)

## Videos
- [Juice it or Lose it](https://www.youtube.com/watch?v=Fy0aCDmgnxg&ab_channel=grapefrukt)
- [Dragon Speech - Chris Crawford](https://www.youtube.com/watch?v=VwZi58u1FjI&ab_channel=GDC)

## Guides
- [Hacking Internships](https://docs.google.com/document/d/1ZNxwXeicOhPm-0E3_DZnwKSq9J3EmW04j1bS6Gvrgy0/edit#)
- [Game Engineer Study Sheet](https://docs.google.com/document/d/1qIkaxtSfrtmgvO_yh-h187by7EIyrl61a3EaF40xBLs/edit#)

## Classes

- [Intro to Game Design and Development - Stanford](https://stanfordgamedev.weebly.com/assignments.html)
- [Understanding Game Engines - CMU](https://courses.ideate.cmu.edu/53-353/f2021/)
- [Game Design, Prototyping and Production - CMU](https://courses.ideate.cmu.edu/53-471/s2021/?page_id=32)

## Books
See PDFs in directory
